package com.kazale.prjcontroller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrjControllerApplication {

	public static void main(String[] args) {
		SpringApplication.run(PrjControllerApplication.class, args);
	}
}
